import { Component, OnInit } from '@angular/core';
import { RecipeListComponent } from './recipes/recipe-list/recipe-list.component'

@Component({
  selector: 'rb-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

  
  constructor(private myRecListComp : RecipeListComponent) { }

  ngOnInit() {
  }

}
