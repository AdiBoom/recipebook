import { Component, OnInit } from '@angular/core';

import { Recipe } from './recipe'

@Component({
  selector: 'rb-recipes',
  templateUrl: './recipes.component.html'
})
export class RecipesComponent implements OnInit {

  // the selected recipe received from the recipeSelected event in the recipe-list component 
  selectedRecipe: Recipe;

  constructor() { }

  ngOnInit() {
  }

}
