import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Recipe } from '../recipe'
import { RecipeItemComponent } from './recipe-item.component'
import { RecipeService } from '../recipe.service'

@Component({
  selector: 'rb-recipe-list',
  templateUrl: './recipe-list.component.html'
})
export class RecipeListComponent implements OnInit {

  recipes: Recipe[] = [];

  // this is defined as Output because the onSelected event gets the recipe as an object and passes on to whom ever uses it.
  // @Output() recipeSelected = new EventEmitter<Recipe>();

  constructor(private recipeService : RecipeService) {}

  ngOnInit() {
    this.recipes = this.recipeService.getRecipes();
  }


  // /**
  //  * outputs the data as received
  //  */
  // onSelected(recipe: Recipe){
  //   this.recipeSelected.emit(recipe)
  // }

}
