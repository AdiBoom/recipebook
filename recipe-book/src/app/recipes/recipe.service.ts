import { Injectable } from '@angular/core';

import { Recipe } from './recipe';
import { Ingredient } from '../shared/ingredient';

@Injectable()
export class RecipeService {

  recipes: Recipe[] = [
    new Recipe('Dummy', 'Dummy', 'http://guides.global/images/guides/global/dummy_web_page.jpg',[
      new Ingredient('Breadcrumbs',1),
      new Ingredient('Chicken brests',1),
      new Ingredient('French fries',20)
    ]),
    new Recipe('Dummy2', 'tomer', 'http://guides.global/images/guides/global/dummy_web_page.jpg',[])
  ];

  constructor() { }

  getRecipes(){
    return this.recipes;
  }

  getRecipe(id:number){
    return this.recipes[id];
  }

  deleteRecipe(recipe:Recipe){
    this.recipes.splice(this.recipes.indexOf(recipe),1);
  }

  addRecipe(recipe : Recipe){
    this.recipes.push(recipe);
  }

  editRecipe(oldRecipe : Recipe, newRecipe : Recipe){
    this.recipes[this.recipes.indexOf(oldRecipe)] = newRecipe;
  }


}
